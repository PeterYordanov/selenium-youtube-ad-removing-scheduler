﻿namespace SeleniumScheduler.Logging
{
    public enum EventType
    {
        ResponseSucceeded,
        ResponseFailed,
        Success,
        Failure,
        SentToDbService
    }
}
