﻿using Newtonsoft.Json.Converters;

namespace SeleniumScheduler.App
{
    public class CheckinDateTimeConverter : IsoDateTimeConverter
    {
        public CheckinDateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }

    public class CheckoutDateTimeConverter : IsoDateTimeConverter
    {
        public CheckoutDateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }

    public class CreatedDateTimeConverter : IsoDateTimeConverter
    {
        public CreatedDateTimeConverter() 
        {
            base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }
    }
}
