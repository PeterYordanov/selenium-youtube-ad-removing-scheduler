﻿using SeleniumScheduler.Quartz;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using SeleniumScheduler.App;
using TBI.SharedLibrary.Logging;
using TBI.SharedLibrary.MessagingQueue;

namespace SeleniumScheduler.Service
{
    //public class Mappings : Profile 
    //{
    //    public Mappings()
    //    {
    //        CreateMap<BDetails, BIDetails>();
    //        CreateMap<BIDetails, IDetails>();
    //        CreateMap<BDetails, IDetails>();

          
    //        CreateMap<BDetails, Item>().ForMember(x => x.Name, opt => opt.NullSubstitute("DummyData"))
    //                                                .ForMember(x => x.Type, opt => opt.NullSubstitute("DummyData"));

    //    }
    //}

    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDependencies(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(new JobSchedule(
                jobType: typeof(SeleniumServiceJob),
                cronExpression: config["ConfigurationOptions:CronExpression"]));

            services.AddScoped<ISeleniumSchedulerManager, SeleniumSchedulerManager>();
            services.AddSingleton<IJobFactory, JobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<QuartzJobRunner>();
            services.AddScoped<ISerilogLogger, SerilogLogger>();
            services.AddScoped<SeleniumServiceJob>();
            services.AddScoped<ISerializationManager, SerializationManager>();
            services.AddHostedService<SeleniumSchedulerService>();
            services.AddScoped<IMessageBroker, RabbitMQBroker>();
            //var mappingConfig = new MapperConfiguration(mc =>
            //{
            //    mc.AddProfile(new Mappings());
            //});

            //IMapper mapper = mappingConfig.CreateMapper();
            //services.AddSingleton(mapper);

            return services;
        }
    }
}
