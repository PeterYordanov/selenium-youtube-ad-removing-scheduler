﻿using Quartz;
using System.Threading.Tasks;
using SeleniumScheduler.App;

namespace SeleniumScheduler.Quartz
{
    public class SeleniumServiceJob : IJob
    {
        private readonly ISeleniumSchedulerManager _seleniumManager;

        public SeleniumServiceJob(ISeleniumSchedulerManager seleniumManager)
        {
            _seleniumManager = seleniumManager;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await _seleniumManager.Run();
        }
    }
}
