﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;
using SeleniumScheduler.Options;
using SeleniumScheduler.Service;
using TBI.SharedLibrary.MessagingQueue;
using SeleniumScheduler.App;
using CommandLine;

namespace SeleniumScheduler
{
    public class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineArguments>(args)
                   .WithParsed(o =>
                   {
                       CreateHost(o).Wait();
                   });
        }

        static async Task CreateHost(CommandLineArguments commandLineArguments)
        {
            switch(commandLineArguments.BrowserType)
            {
                case 1:
                    SeleniumSingleton.Instance();
                    break;
                case 2:
                    SeleniumSingleton.Instance(TBI.SharedLibrary.Enumerations.Browsers.Firefox);
                    break;
                default:
                    break;
            }

            var host = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    services.AddOptions<ConfigurationOptions>().Configure(options => configuration.GetSection(nameof(ConfigurationOptions)).Bind(options));
                    services.AddOptions<MessageBrokerOptions>().Configure(options => configuration.GetSection(nameof(MessageBrokerOptions)).Bind(options));
                    services.AddDependencies(configuration);
                })
                .ConfigureAppConfiguration((context, builder) =>
                {
                    //string environment = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");
                    //context.HostingEnvironment.EnvironmentName = environment;

                    builder.SetBasePath(AppContext.BaseDirectory)
                        .AddJsonFile($"appsettings.json", true, true);
                })
                .UseConsoleLifetime()
                .Build();

            await host.RunAsync();
        }
    }
}
