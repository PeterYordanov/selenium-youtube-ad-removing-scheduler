﻿using System.Net.Http;
using TBI.SharedLibrary.Logging;

namespace SeleniumScheduler.Logging
{
    public class LogModelExtended : LogModel
    {
        public EventType EventType { get; set; }

        public HttpResponseMessage ResponseMessage { get; set; }

        public string InnerException { get; set; }
    }
}
