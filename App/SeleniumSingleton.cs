﻿using TBI.SharedLibrary.Enumerations;
using TBI.SharedLibrary.Infrastructure;

namespace SeleniumScheduler.App
{
    public static class SeleniumSingleton
    {
        private static WebDriverFacade WebDriverFacade = null;

        public static WebDriverFacade Instance(Browsers type = Browsers.Chrome)
        {
            if(WebDriverFacade == null)
            {
                WebDriverFacade = new WebDriverFacade(type);
                WebDriverFacade.GoToUrl("https://google.com");
            }

            return WebDriverFacade;
        }
    }
}
