﻿using CommandLine;

namespace SeleniumScheduler.Options
{
    public class CommandLineArguments
    {
        [Option('t', "browser_type", Required = false, HelpText = "What type of browser to create (1 for Chrome, 2 for Firefox)", Default = 1)]
        public int BrowserType { get; set; }
    }
}
