﻿namespace SeleniumScheduler.Options
{
    public class ConfigurationOptions
    {
        public string CronExpression { get; set; }
    }
}