﻿using System.Threading.Tasks;
using TBI.SharedLibrary.Logging;
using TBI.SharedLibrary.Infrastructure;
using OpenQA.Selenium;
using System;
using SeleniumScheduler.Logging;

namespace SeleniumScheduler.App
{
    public interface ISeleniumSchedulerManager
    {
        Task Run();
    }

    public class SeleniumSchedulerManager : ISeleniumSchedulerManager
    {
        private readonly ISerilogLogger _logger;
        private readonly WebDriverFacade webDriverFacade;

        public SeleniumSchedulerManager(ISerilogLogger logger)
        {
            webDriverFacade = SeleniumSingleton.Instance();
            _logger = logger;
        }

        public Task Run()
        {
            try
            {
                string skipAdButton = "//div[starts-with(@id,'skip-button')]/span/button";
                By bySkipAdButton = By.XPath(skipAdButton);
                if (webDriverFacade.IsElementPresent(bySkipAdButton) && webDriverFacade.IsElementDisplayed(bySkipAdButton))
                {
                    webDriverFacade.FindElement(bySkipAdButton).Click();
                    _logger.LogInfo(new LogModelExtended
                    {
                        Message = "Close ad button (Type 1) clicked",
                        LogCategory = LogCategory.Information
                    });
                }

                string skipAdButton2 = "//div[starts-with(@id,'ad-text')]";
                By bySkipAdButton2 = By.XPath(skipAdButton2);
                if (webDriverFacade.IsElementPresent(bySkipAdButton2) && webDriverFacade.IsElementDisplayed(bySkipAdButton2))
                {
                    webDriverFacade.FindElement(bySkipAdButton2).Click();
                    _logger.LogInfo(new LogModelExtended
                    {
                        Message = "Close ad button (Type 2) clicked",
                        LogCategory = LogCategory.Information
                    });
                }

                string closeOverlayAd = "//div[starts-with(@id,'invideo-overlay')]/div/div[3]/div[2]/button";
                By byCloseOverlayAd = By.XPath(closeOverlayAd);
                if (webDriverFacade.IsElementPresent(byCloseOverlayAd) && webDriverFacade.IsElementDisplayed(byCloseOverlayAd))
                {
                    webDriverFacade.FindElement(byCloseOverlayAd).Click();
                    _logger.LogInfo(new LogModelExtended
                    {
                        Message = "Ad overlay (Type 1) button clicked",
                        LogCategory = LogCategory.Information
                    });
                }

                string closeOverlayAd2 = "//div[starts-with(@id,'invideo-overlay')]/div/div/div[2]/button";
                By byCloseOverlayAd2 = By.XPath(closeOverlayAd2);
                if (webDriverFacade.IsElementPresent(byCloseOverlayAd2) && webDriverFacade.IsElementDisplayed(byCloseOverlayAd2))
                {
                    webDriverFacade.FindElement(byCloseOverlayAd2).Click();
                    _logger.LogInfo(new LogModelExtended
                    {
                        Message = "Ad overlay (Type 2) button clicked",
                        LogCategory = LogCategory.Information
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(new LogModelExtended
                {
                    InnerException = ex.InnerException?.Message,
                    Message = ex.Message,
                    BaseException = ex.GetBaseException().Message,
                    StackTrace = ex.StackTrace,
                    LogCategory = LogCategory.Error
                });
            }

            return Task.CompletedTask;
        }
    }
}
